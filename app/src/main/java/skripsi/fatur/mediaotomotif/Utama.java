package skripsi.fatur.mediaotomotif;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.stephentuso.welcome.WelcomeHelper;

import java.util.ArrayList;
import java.util.List;

import skripsi.fatur.mediaotomotif.Detail.Tutorial;
import skripsi.fatur.mediaotomotif.Fragment.Menu1_Profile;
import skripsi.fatur.mediaotomotif.Fragment.Menu2_Video;
import skripsi.fatur.mediaotomotif.Fragment.Menu3_Home;
import skripsi.fatur.mediaotomotif.Fragment.Menu4_Jobsheet;
import skripsi.fatur.mediaotomotif.Fragment.Menu5_Evaluasi;
import toan.android.floatingactionmenu.FloatingActionButton;

public class Utama extends AppCompatActivity implements View.OnClickListener{

    private TabLayout tabLayout;
    FloatingActionButton menu3;
    private ViewPager viewPager;

    LinearLayout menu1,menu2,menu4,menu5;
    ImageView img1,img2,img4,img5;
    TabLayout.Tab tab;
    TextView judul;
    int press = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_utama);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        menu3 = (FloatingActionButton) findViewById(R.id.menu3);
        menu1 = (LinearLayout) findViewById(R.id.menu1);
        menu2 = (LinearLayout) findViewById(R.id.menu2);
        menu4 = (LinearLayout) findViewById(R.id.menu4);
        menu5 = (LinearLayout) findViewById(R.id.menu5);
        menu1.setOnClickListener(this);
        menu2.setOnClickListener(this);
        menu3.setOnClickListener(this);
        menu4.setOnClickListener(this);
        menu5.setOnClickListener(this);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img4 = (ImageView) findViewById(R.id.img4);
        img5 = (ImageView) findViewById(R.id.img5);
        judul = (TextView) findViewById(R.id.judul);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if(tab.getPosition()==0)
                {
                    img1.setImageResource(R.drawable.menu1b);
                    img2.setImageResource(R.drawable.menu2a);
                    menu3.setIcon(R.drawable.menu3a);
                    img4.setImageResource(R.drawable.menu4a);
                    img5.setImageResource(R.drawable.menu5a);
                    YoYo.with(Techniques.BounceInDown)
                            .duration(1000)
                            .playOn(findViewById(R.id.judul));
                    judul.setText(getResources().getString(R.string.menu1));
                }
                else if(tab.getPosition()==1)
                {
                    img1.setImageResource(R.drawable.menu1a);
                    img2.setImageResource(R.drawable.menu2b);
                    menu3.setIcon(R.drawable.menu3a);
                    img4.setImageResource(R.drawable.menu4a);
                    img5.setImageResource(R.drawable.menu5a);
                    judul.setText(getResources().getString(R.string.menu2));
                    YoYo.with(Techniques.BounceInDown)
                            .duration(1000)
                            .playOn(findViewById(R.id.judul));
                    judul.setText(getResources().getString(R.string.menu2));
                }
                else if(tab.getPosition()==2)
                {
                    img1.setImageResource(R.drawable.menu1a);
                    img2.setImageResource(R.drawable.menu2a);
                    menu3.setIcon(R.drawable.menu3b);
                    img4.setImageResource(R.drawable.menu4a);
                    img5.setImageResource(R.drawable.menu5a);
                    judul.setText(getResources().getString(R.string.menu3));
                    YoYo.with(Techniques.BounceInDown)
                            .duration(1000)
                            .playOn(findViewById(R.id.judul));
                    judul.setText(getResources().getString(R.string.menu3));
                }
                else if(tab.getPosition()==3)
                {
                    img1.setImageResource(R.drawable.menu1a);
                    img2.setImageResource(R.drawable.menu2a);
                    menu3.setIcon(R.drawable.menu3a);
                    img4.setImageResource(R.drawable.menu4b);
                    img5.setImageResource(R.drawable.menu5a);
                    judul.setText(getResources().getString(R.string.menu4));
                    YoYo.with(Techniques.BounceInDown)
                            .duration(1000)
                            .playOn(findViewById(R.id.judul));
                    judul.setText(getResources().getString(R.string.menu4));
                }
                else if(tab.getPosition()==4)
                {
                    img1.setImageResource(R.drawable.menu1a);
                    img2.setImageResource(R.drawable.menu2a);
                    menu3.setIcon(R.drawable.menu3a);
                    img4.setImageResource(R.drawable.menu4a);
                    img5.setImageResource(R.drawable.menu5b);
                    judul.setText(getResources().getString(R.string.menu5));
                    YoYo.with(Techniques.BounceInDown)
                            .duration(1000)
                            .playOn(findViewById(R.id.judul));
                    judul.setText(getResources().getString(R.string.menu5));
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //tab.setIcon(tabIcons[tab.getPosition()]);


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tab = tabLayout.getTabAt(2);
        tab.select();


        WelcomeHelper welcomeScreen = new WelcomeHelper(this, Tutorial.class);
        welcomeScreen.show(savedInstanceState);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Menu1_Profile(), "");
        adapter.addFragment(new Menu2_Video(), "");
        adapter.addFragment(new Menu3_Home(), "");
        adapter.addFragment(new Menu4_Jobsheet(), "");
        adapter.addFragment(new Menu5_Evaluasi(), "");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.menu1:
                tab = tabLayout.getTabAt(0);
                tab.select();
                break;
            case R.id.menu2:
                tab = tabLayout.getTabAt(1);
                tab.select();
                break;
            case R.id.menu3:
                tab = tabLayout.getTabAt(2);
                tab.select();
                break;
            case R.id.menu4:
                tab = tabLayout.getTabAt(3);
                tab.select();
                break;
            case R.id.menu5:
                tab = tabLayout.getTabAt(4);
                tab.select();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);
            return null;
        }
    }


    @Override
    public void onBackPressed() {
        if(press==0)
        {
            Toast.makeText(getApplicationContext(), "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
            press = 1;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    press = 0;
                }
            },3000);
        }
        else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            press = 0;
        }
    }
}