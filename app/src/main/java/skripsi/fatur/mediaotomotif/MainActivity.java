package skripsi.fatur.mediaotomotif;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.orm.SugarContext;
import com.snatik.storage.Storage;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import skripsi.fatur.mediaotomotif.Database.Data_Cache;

public class MainActivity extends AppCompatActivity {

    /*RopeProgressBar mRopeProgressBar;
    CountDownTimer countDownTimer;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        AVLoadingIndicatorView avi = findViewById(R.id.avi);
        avi.show();
        //mRopeProgressBar = findViewById(R.id.ropeProgress);
        SugarContext.init(this);
        Data_Cache data = Data_Cache.findById(Data_Cache.class,1L);
        if((int)data.count(Data_Cache.class, "", null) == 0) {

            Data_Cache data3 = new Data_Cache("0");
            data3.save();
        }


        /*Storage storage = new Storage(getApplicationContext());
        String path1 = "android.resource://"
                + getPackageName()
                + "/"
                + R.raw.sheet;
        String path2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/sheet.pdf";
        storage.copy(path1, path2);*/

        /*Uri uri = Uri.parse(
                "android.resource://"
                        + getPackageName()
                        + "/"
                        + R.raw.sheet);

        String sourceFilename= uri.getPath();
        String destinationFilename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/sheet.pdf";

        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(sourceFilename));
            bos = new BufferedOutputStream(new FileOutputStream(destinationFilename, false));
            byte[] buf = new byte[1024];
            bis.read(buf);
            do {
                bos.write(buf);
            } while(bis.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/

        /*final int[] mSongs = new int[] { R.raw.suara1};
        for (int i = 0; i < mSongs.length; i++) {
            try {
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "";
                File dir = new File(path);
                String str_song_name = i + ".mp3";
                CopyRAWtoSDCard(mSongs[i], path + File.separator + str_song_name);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Error",""+e.getMessage());
            }
        }*/

        Handler handler =  new Handler();
        Runnable myRunnable = new Runnable() {
            public void run() {
                // do something
                Intent intent = new Intent(MainActivity.this,Utama.class);
                startActivity(intent);
            }
        };
        handler.postDelayed(myRunnable,3000);

        /*countDownTimer = new CountDownTimer(3000, 30) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                int set = (int) millisUntilFinished/30;
                int set2 = 100 - set;
                mRopeProgressBar.setProgress(set2);

            }

            public void onFinish()
            {
                Intent intent = new Intent(MainActivity.this,Utama.class);
                startActivity(intent);
            }
        };
        countDownTimer.start();*/
    }

    private void CopyRAWtoSDCard(int id, String path) throws IOException {
        InputStream in = getResources().openRawResource(id);
        FileOutputStream out = new FileOutputStream(path);
        byte[] buff = new byte[1024];
        int read = 0;
        try {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } finally {
            in.close();
            out.close();
        }
    }

}
