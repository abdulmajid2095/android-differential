package skripsi.fatur.mediaotomotif.Detail;

import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import skripsi.fatur.mediaotomotif.R;

public class Video_Play extends AppCompatActivity {

    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video__play);

        VideoView v4 = (VideoView)findViewById(R.id.v1);
        String videoku = getIntent().getStringExtra("video");
        if(videoku.equals("1"))
        {
            uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video1);
        }
        else if(videoku.equals("2"))
        {
            uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video2);
        }
        else if(videoku.equals("3"))
        {
            uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video3);
        }
        else
        {
            uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video4);
        }
        v4.setVideoURI(uri);
        v4.setMediaController(new MediaController(this));
        v4.requestFocus();
        v4.start();
    }
}
