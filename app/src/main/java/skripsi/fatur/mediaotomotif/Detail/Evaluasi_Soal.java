package skripsi.fatur.mediaotomotif.Detail;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.otaliastudios.zoom.ZoomImageView;

import java.util.concurrent.TimeUnit;

import skripsi.fatur.mediaotomotif.Database.Data_Cache;
import skripsi.fatur.mediaotomotif.R;
import skripsi.fatur.mediaotomotif.Utama;

public class Evaluasi_Soal extends AppCompatActivity {
    Toolbar toolbar;

   String[] soalku = {"Komponen utama pada differential terdiri dari dua bagian yaitu...",
                    "Berikut ini yang mferupakan penjelasan dari sistem diferensial adalah...",
                    "Yang ditunjuk angka no 4 adalah komponen...",
                    "Yang ditunjuk angka no 2 adalah komponen...",
                    "Yang ditunjuk angka no 5 adalah komponen...",
                    "Yang ditunjuk angka no 3 adalah komponen...",
                    "Yang ditunjuk angka no 6 adalah komponen...",
                    "Yang ditunjuk angka no 1 adalah komponen...",
                    "Yang ditunjuk angka no 7 adalah komponen...",
                    "Salah satu fungsi Differential case adalah...",

                    "Yang meneruskan tenaga putar roda dari drive gear  menuju ke exle shaft adalah...",
                    "Fungsi pinion gear adalah...",
                    "Fungsi ring gear adalah...",
                    "Yang meneruskan putaran dari propeler shaft ke ring gear adalah...",
                    "Yang berfungsi sebagai rumah dudukan dari differential gear adalah...",
                    "Fungsi exle shaft adalah...",
                    "Fungsi differential di bawah ini kecuali...",
                    "Cara kerja differential saat kendaraan berbelok ke kiri adalah...",
                    "Cara kerja differential saat kendaraan berbelok ke kanan adalah...",
                    "Ketika kendaraan berjalan di jalan yang lurus, maka cara kerjanya adalah...",

                    "Hubungan antara rpm roda penggerak dengan roda gigi korona dapat diuraikan sbb:",
                    "Pemeriksaan yang dapat dilakukan secara visual/pengamatan pada saat final drive/gardan terpasang pada kendaraan adalah...",
                    "Gambar di bawah ini menunjukkan pemeriksaan gardan (differential). Bagian yang diperiksa adalah...",
                    "Berapakah standar toleransi dari backlash side gear ?",
                    "Pengukuran pada final drive/gardan yang tidak dapat dilakukan menggunakan DTI (Dial Test Indicator) adalah...",
                    "Gambar di bawah ini menunjukkan pemeriksaan gardan (differential). Bagian yang diperiksa adalah...",
                    "Alat yang digunakan paling tepat untuk memeriksa preload dibawah ini adalah ?",
                    "Alat apa saja yang digunakan untuk memeriksa backlash ring gear ?",
                    "Gambar di bawah ini menunjukkan pemeriksaan gardan (differential). Bagian yang diperiksa adalah...",
                    "Berapakah standar toleransi dari backlash ring gear ?",

                    "Pada saat melakukan pemeriksaan keolengan (Run Out) Ring gear, standarnya adalah...",
                    "Gambar di bawah ini menunjukkan pemeriksaan gardan (differential). Bagian yang diperiksa adalah...",
                    "Gambar di bawah ini adalah pemeriksaan diferensial. Apa nama pemeriksaan tersebut ?",
                    "Alat apa  yang digunakan untuk memeriksa Celah antara side gear dengan diferensial case ?"
    };

    String[] kunci = {"b","c","b","c","d",
                        "b","a","c","a","c",
                        "d","d","a","a","d",
                        "b","c","b","a","d",
                        "b","d","c","c","a",
                        "b","d","b","a","c",
                        "c","d","b","a"};

    String[] jawaban = {"","","","","",
                        "","","","","",
                        "","","","","",
                        "","","","","",
                        "","","","","",
                        "","","","","",
                        "","","",""};

    String[] opsiA = {"Final Drive dan defrensial gear",
                        "Sistem yang berfungsi membedakan putaran roda depan dan belakang",
                        "Defrensial carrier",
                        "Defrensial carrier",
                        "Defrensial carrier",
                        "Defrensial carrier",
                        "Defrensial carrier",
                        "Defrensial carrier",
                        "Exle shaft",
                        "Meneruskan putaran dari drive gear menuju ke differential gear melalui differential case",

                        "Defrensial carrier",
                        "Meneruskan putaran dari drive gear menuju ke differential gear melalui differential case",
                        "Meneruskan putaran dari drive gear menuju ke differential gear melalui differential case",
                        "Drive pinion",
                        "Side gear",
                        "Meneruskan putaran dari drive gear menuju ke differential gear melalui differential case",
                        "Membedakan putaran saat berbelok",
                        "Pada saat kendaraan berbelok ke kiri, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kiri berputar lebih cepat, pinion gear berputar mengelilingi side gear yang tahanannya lebih besar",
                        "Pada saat kendaraan berbelok ke kanan, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kanan berputar lebih lambat, pinion gear berputar mengelilingi side gear yang tahanannya lebih besar",
                        "Ring gear – differensial case – side gear – drive pinion – ring gear",

                        "OPSIII",
                        "Suara yang timbul akibat gesekan",
                        "Keausan ring gear",
                        "0,02mm- 0,05mm",
                        "Keausan bantalan",
                        "Keausan bantalan",
                        "Oli bekas",
                        "Cylinder bore gauge",
                        "Backlash ring gear",
                        "0,05cm- 0,07cm",

                        "0,13 mm – 0,18 mm",
                        "Backlash ring gear",
                        "Celah antara gigi ring gear dengan rumah diferensial",
                        "Fueller gauge"};

    String[] opsiB = {"Final gear dan defrensial gear",
                        "Sistem yang berfungsi mengatur rasio putaran mesin",
                        "Pinion gear",
                        "Pinion gear",
                        "Pinion gear",
                        "Drive pinion",
                        "Pinion gear",
                        "Pinion gear",
                        "Pinion gear",
                        "Meneruskan putaran dari propeler shaft ke ring gear",

                        "Gear pinion",
                        "Meneruskan putaran dari propeler shaft ke ring gear",
                        "Meneruskan putaran dari propeler shaft ke ring gear",
                        "Pinion gear",
                        "Pinion gear",
                        "Meneruskan putaran dari side gear ke ban",
                        "Mencegah terjadinya selip saat perbedaan permukaan jalan",
                        "Pada saat kendaraan berbelok ke kiri, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kiri berputar lebih lambat, pinion gear berputar mengelilingi side gear yang tahanannya lebih besar",
                        "Pada saat kendaraan berbelok ke kanan, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kanan berputar lebih lambat, pinion gear berputar mengelilingi side gear yang tahanannya lebih kecil",
                        "Axle shaft – drive pinion – ring gear – differensial case – side gear",

                        "OPSII",
                        "Keausan bantalan",
                        "Run out (kerataan permukaan putar ring gear)",
                        "0,2mm- 0,5mm",
                        "Run out ring gear",
                        "Penyetelan preload drive pinion",
                        "Pencil",
                        "Dial gauge",
                        "Penyetelan preload drive pinion",
                        "0,05- 0,07mm",

                        "0,05 cm - 0,07cm",
                        "Penyetelan preload drive pinion",
                        "Celah antara side gear dengan diferensial case",
                        "Dial gauge"};

    String[] opsiC = {"Defrensial case dan pinion shaft",
                        "Sistem yang berfungsi membedakan putaran roda kanan dan kiri",
                        "Ring gear",
                        "Ring gear",
                        "Ring gear",
                        "Ring gear",
                        "Ring gear",
                        "Propeler shaft",
                        "Ring gear",
                        "Rumah dudukan dari differential gear",

                        "Ring gear",
                        "Meneruskan tenaga putar roda dari drive gear menuju ke exle shaft",
                        "Rumah dudukan dari differential gear",
                        "Ring gear",
                        "Ring gear",
                        "Rumah dudukan dari differential gear",
                        "Mengatur perpindahan gigi",
                        "Pada saat kendaraan berbelok ke kiri, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kiri berputar lebih lambat, pinion gear berputar mengelilingi side gear yang tahanannya lebih kecil",
                        "Pada saat kendaraan berbelok ke kanan, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kanan berputar lebih cepat, pinion gear berputar mengelilingi side gear yang tahanannya lebih besar",
                        "Drive pinion – ring gear – side gear – differensial case – axle shaft",

                        "OPSII",
                        "Kerusakan ring gear",
                        "Backlash side gear (gigi samping)",
                        "0,05mm- 0,20mm",
                        "Backlash ring gear",
                        "Backlash ring gear",
                        "Kapur",
                        "Vernier caliper",
                        "Keausan bantalan",
                        "0,13mm- 0,18mm",

                        "0,05mm- 0,07mm",
                        "Keausan bantalan",
                        "Celah antara gigi korona dengan side gear",
                        "Vernier caliper"};

    String[] opsiD = {"Drive pinion dan defrensial pinion",
                        "Sistem yang berfungsi menyambung dan memutuskan putaran mesin ke transmisi",
                        "Side gear",
                        "Side gear",
                        "Side gear",
                        "Side gear",
                        "Side gear",
                        "Ring gear",
                        "Side gear",
                        "Meneruskan tenaga putar roda dari drive gear  menuju ke exle shaft",

                        "Side gear",
                        "Berfungsi untuk membedakan putaran roda kiri dan kanan pada saat kendaraan berbelok",
                        "Meneruskan tenaga putar roda dari drive gear menuju ke exle shaft",
                        "Side gear",
                        "Deferensial carrier",
                        "Meneruskan putaran dari propeler shaft ke ring gear",
                        "Meneruskan putaran dari poros propeler ke exle shaft",
                        "Pada saat kendaraan berbelok ke kiri, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kiri berputar lebih cepat, pinion gear berputar mengelilingi side gear yang tahanannya lebih kecil",
                        "Pada saat kendaraan berbelok ke kanan, putaran dari kedua roda berbeda, tahananya pun berbeda, sisi roda kanan berputar lebih cepat, pinion gear berputar mengelilingi side gear yang tahanannya lebih kecil",
                        "Drive pinion – ring gear – differensial case – side gear – axle shaft",

                        "OPSII",
                        "Kebocoran oli akibat seal aus",
                        "Backlash ring gear",
                        "0,5 mm- 0,20mm",
                        "Run out side gear",
                        "Run out side gear",
                        "Grasee",
                        "Micrometer",
                        "Run out side gear",
                        "0,13cm- 0,18cm",

                        "0,13 cm – 0,18 cm",
                        "Run out ring gear",
                        "Celah antara pinion gear dengan side gear",
                        "Micrometer"};

    TextView textMinute,textSecond;
    Button selesai;
    CountDownTimer countDownTimer;
    int jumlah_benar = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_evaluasi__soal);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textMinute = findViewById(R.id.text_minute);
        textSecond = findViewById(R.id.text_second);

        countDownTimer = new CountDownTimer(1801000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                textMinute.setText(""+String.format("%d", TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished)));
                textSecond.setText(""+String.format("%d", TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish()
            {
                textMinute.setText("00");
                textSecond.setText("00");
                dialogTimesUp();
            }
        };
        countDownTimer.start();

        selesai = findViewById(R.id.selesai);
        selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_selesai();
            }
        });

        setUpSoal1();
        setUpSoal2();
        setUpSoal3();
        setUpSoal4();
        setUpSoal5();
        setUpSoal6();
        setUpSoal7();
        setUpSoal8();
        setUpSoal9();
        setUpSoal10();
        setUpSoal11();
        setUpSoal12();
        setUpSoal13();
        setUpSoal14();
        setUpSoal15();
        setUpSoal16();
        setUpSoal17();
        setUpSoal18();
        setUpSoal19();
        setUpSoal20();
        setUpSoal21();
        setUpSoal22();
        setUpSoal23();
        setUpSoal24();
        setUpSoal25();
        setUpSoal26();
        setUpSoal27();
        setUpSoal28();
        setUpSoal29();
        setUpSoal30();
        setUpSoal31();
        setUpSoal32();
        setUpSoal33();
        setUpSoal34();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void setUpSoal1()
    {
        TextView soal = findViewById(R.id.soal1);
        TextView opsi1a = findViewById(R.id.opsi1a);
        TextView opsi1b = findViewById(R.id.opsi1b);
        TextView opsi1c = findViewById(R.id.opsi1c);
        TextView opsi1d = findViewById(R.id.opsi1d);
        final RadioButton radio1a = findViewById(R.id.radio1a);
        final RadioButton radio1b = findViewById(R.id.radio1b);
        final RadioButton radio1c = findViewById(R.id.radio1c);
        final RadioButton radio1d = findViewById(R.id.radio1d);

        soal.setText(""+soalku[0]);
        opsi1a.setText(""+opsiA[0]);
        opsi1b.setText(""+opsiB[0]);
        opsi1c.setText(""+opsiC[0]);
        opsi1d.setText(""+opsiD[0]);

        radio1a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio1a.setChecked(true);
                radio1b.setChecked(false);
                radio1c.setChecked(false);
                radio1d.setChecked(false);
                jawaban[0] = "a";
            }
        });
        radio1b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio1a.setChecked(false);
                radio1b.setChecked(true);
                radio1c.setChecked(false);
                radio1d.setChecked(false);
                jawaban[0] = "b";
            }
        });
        radio1c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio1a.setChecked(false);
                radio1b.setChecked(false);
                radio1c.setChecked(true);
                radio1d.setChecked(false);
                jawaban[0] = "c";
            }
        });
        radio1d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio1a.setChecked(false);
                radio1b.setChecked(false);
                radio1c.setChecked(false);
                radio1d.setChecked(true);
                jawaban[0] = "d";
            }
        });
    }

    public void setUpSoal2()
    {
        TextView soal = findViewById(R.id.soal2);
        TextView opsi2a = findViewById(R.id.opsi2a);
        TextView opsi2b = findViewById(R.id.opsi2b);
        TextView opsi2c = findViewById(R.id.opsi2c);
        TextView opsi2d = findViewById(R.id.opsi2d);
        final RadioButton radio2a = findViewById(R.id.radio2a);
        final RadioButton radio2b = findViewById(R.id.radio2b);
        final RadioButton radio2c = findViewById(R.id.radio2c);
        final RadioButton radio2d = findViewById(R.id.radio2d);

        soal.setText(""+soalku[1]);
        opsi2a.setText(""+opsiA[1]);
        opsi2b.setText(""+opsiB[1]);
        opsi2c.setText(""+opsiC[1]);
        opsi2d.setText(""+opsiD[1]);

        radio2a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio2a.setChecked(true);
                radio2b.setChecked(false);
                radio2c.setChecked(false);
                radio2d.setChecked(false);
                jawaban[1] = "a";
            }
        });
        radio2b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio2a.setChecked(false);
                radio2b.setChecked(true);
                radio2c.setChecked(false);
                radio2d.setChecked(false);
                jawaban[1] = "b";
            }
        });
        radio2c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio2a.setChecked(false);
                radio2b.setChecked(false);
                radio2c.setChecked(true);
                radio2d.setChecked(false);
                jawaban[1] = "c";
            }
        });
        radio2d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio2a.setChecked(false);
                radio2b.setChecked(false);
                radio2c.setChecked(false);
                radio2d.setChecked(true);
                jawaban[1] = "d";
            }
        });
    }

    public void setUpSoal3()
    {
        final ZoomImageView zoomImage = findViewById(R.id.zoom_image);
        Drawable img = getResources().getDrawable( R.drawable.soalgb1);
        zoomImage.setImageDrawable(img);

        TextView soal = findViewById(R.id.soal3);
        TextView opsi3a = findViewById(R.id.opsi3a);
        TextView opsi3b = findViewById(R.id.opsi3b);
        TextView opsi3c = findViewById(R.id.opsi3c);
        TextView opsi3d = findViewById(R.id.opsi3d);
        final RadioButton radio3a = findViewById(R.id.radio3a);
        final RadioButton radio3b = findViewById(R.id.radio3b);
        final RadioButton radio3c = findViewById(R.id.radio3c);
        final RadioButton radio3d = findViewById(R.id.radio3d);

        soal.setText(""+soalku[2]);
        opsi3a.setText(""+opsiA[2]);
        opsi3b.setText(""+opsiB[2]);
        opsi3c.setText(""+opsiC[2]);
        opsi3d.setText(""+opsiD[2]);

        radio3a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio3a.setChecked(true);
                radio3b.setChecked(false);
                radio3c.setChecked(false);
                radio3d.setChecked(false);
                jawaban[2] = "a";
            }
        });
        radio3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio3a.setChecked(false);
                radio3b.setChecked(true);
                radio3c.setChecked(false);
                radio3d.setChecked(false);
                jawaban[2] = "b";
            }
        });
        radio3c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio3a.setChecked(false);
                radio3b.setChecked(false);
                radio3c.setChecked(true);
                radio3d.setChecked(false);
                jawaban[2] = "c";
            }
        });
        radio3d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio3a.setChecked(false);
                radio3b.setChecked(false);
                radio3c.setChecked(false);
                radio3d.setChecked(true);
                jawaban[2] = "d";
            }
        });
    }

    public void setUpSoal4()
    {
        TextView soal = findViewById(R.id.soal4);
        TextView opsi4a = findViewById(R.id.opsi4a);
        TextView opsi4b = findViewById(R.id.opsi4b);
        TextView opsi4c = findViewById(R.id.opsi4c);
        TextView opsi4d = findViewById(R.id.opsi4d);
        final RadioButton radio4a = findViewById(R.id.radio4a);
        final RadioButton radio4b = findViewById(R.id.radio4b);
        final RadioButton radio4c = findViewById(R.id.radio4c);
        final RadioButton radio4d = findViewById(R.id.radio4d);

        soal.setText(""+soalku[3]);
        opsi4a.setText(""+opsiA[3]);
        opsi4b.setText(""+opsiB[3]);
        opsi4c.setText(""+opsiC[3]);
        opsi4d.setText(""+opsiD[3]);

        radio4a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio4a.setChecked(true);
                radio4b.setChecked(false);
                radio4c.setChecked(false);
                radio4d.setChecked(false);
                jawaban[3] = "a";
            }
        });
        radio4b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio4a.setChecked(false);
                radio4b.setChecked(true);
                radio4c.setChecked(false);
                radio4d.setChecked(false);
                jawaban[3] = "b";
            }
        });
        radio4c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio4a.setChecked(false);
                radio4b.setChecked(false);
                radio4c.setChecked(true);
                radio4d.setChecked(false);
                jawaban[3] = "c";
            }
        });
        radio4d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio4a.setChecked(false);
                radio4b.setChecked(false);
                radio4c.setChecked(false);
                radio4d.setChecked(true);
                jawaban[3] = "d";
            }
        });
    }

    public void setUpSoal5()
    {
        TextView soal = findViewById(R.id.soal5);
        TextView opsi5a = findViewById(R.id.opsi5a);
        TextView opsi5b = findViewById(R.id.opsi5b);
        TextView opsi5c = findViewById(R.id.opsi5c);
        TextView opsi5d = findViewById(R.id.opsi5d);
        final RadioButton radio5a = findViewById(R.id.radio5a);
        final RadioButton radio5b = findViewById(R.id.radio5b);
        final RadioButton radio5c = findViewById(R.id.radio5c);
        final RadioButton radio5d = findViewById(R.id.radio5d);

        soal.setText(""+soalku[4]);
        opsi5a.setText(""+opsiA[4]);
        opsi5b.setText(""+opsiB[4]);
        opsi5c.setText(""+opsiC[4]);
        opsi5d.setText(""+opsiD[4]);

        radio5a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio5a.setChecked(true);
                radio5b.setChecked(false);
                radio5c.setChecked(false);
                radio5d.setChecked(false);
                jawaban[4] = "a";
            }
        });
        radio5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio5a.setChecked(false);
                radio5b.setChecked(true);
                radio5c.setChecked(false);
                radio5d.setChecked(false);
                jawaban[4] = "b";
            }
        });
        radio5c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio5a.setChecked(false);
                radio5b.setChecked(false);
                radio5c.setChecked(true);
                radio5d.setChecked(false);
                jawaban[4] = "c";
            }
        });
        radio5d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio5a.setChecked(false);
                radio5b.setChecked(false);
                radio5c.setChecked(false);
                radio5d.setChecked(true);
                jawaban[4] = "d";
            }
        });
    }

    public void setUpSoal6()
    {
        TextView soal = findViewById(R.id.soal6);
        TextView opsi6a = findViewById(R.id.opsi6a);
        TextView opsi6b = findViewById(R.id.opsi6b);
        TextView opsi6c = findViewById(R.id.opsi6c);
        TextView opsi6d = findViewById(R.id.opsi6d);
        final RadioButton radio6a = findViewById(R.id.radio6a);
        final RadioButton radio6b = findViewById(R.id.radio6b);
        final RadioButton radio6c = findViewById(R.id.radio6c);
        final RadioButton radio6d = findViewById(R.id.radio6d);

        soal.setText(""+soalku[5]);
        opsi6a.setText(""+opsiA[5]);
        opsi6b.setText(""+opsiB[5]);
        opsi6c.setText(""+opsiC[5]);
        opsi6d.setText(""+opsiD[5]);

        radio6a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio6a.setChecked(true);
                radio6b.setChecked(false);
                radio6c.setChecked(false);
                radio6d.setChecked(false);
                jawaban[5] = "a";
            }
        });
        radio6b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio6a.setChecked(false);
                radio6b.setChecked(true);
                radio6c.setChecked(false);
                radio6d.setChecked(false);
                jawaban[5] = "b";
            }
        });
        radio6c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio6a.setChecked(false);
                radio6b.setChecked(false);
                radio6c.setChecked(true);
                radio6d.setChecked(false);
                jawaban[5] = "c";
            }
        });
        radio6d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio6a.setChecked(false);
                radio6b.setChecked(false);
                radio6c.setChecked(false);
                radio6d.setChecked(true);
                jawaban[5] = "d";
            }
        });
    }

    public void setUpSoal7()
    {
        TextView soal = findViewById(R.id.soal7);
        TextView opsi7a = findViewById(R.id.opsi7a);
        TextView opsi7b = findViewById(R.id.opsi7b);
        TextView opsi7c = findViewById(R.id.opsi7c);
        TextView opsi7d = findViewById(R.id.opsi7d);
        final RadioButton radio7a = findViewById(R.id.radio7a);
        final RadioButton radio7b = findViewById(R.id.radio7b);
        final RadioButton radio7c = findViewById(R.id.radio7c);
        final RadioButton radio7d = findViewById(R.id.radio7d);

        soal.setText(""+soalku[6]);
        opsi7a.setText(""+opsiA[6]);
        opsi7b.setText(""+opsiB[6]);
        opsi7c.setText(""+opsiC[6]);
        opsi7d.setText(""+opsiD[6]);

        radio7a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio7a.setChecked(true);
                radio7b.setChecked(false);
                radio7c.setChecked(false);
                radio7d.setChecked(false);
                jawaban[6] = "a";
            }
        });
        radio7b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio7a.setChecked(false);
                radio7b.setChecked(true);
                radio7c.setChecked(false);
                radio7d.setChecked(false);
                jawaban[6] = "b";
            }
        });
        radio7c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio7a.setChecked(false);
                radio7b.setChecked(false);
                radio7c.setChecked(true);
                radio7d.setChecked(false);
                jawaban[6] = "c";
            }
        });
        radio7d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio7a.setChecked(false);
                radio7b.setChecked(false);
                radio7c.setChecked(false);
                radio7d.setChecked(true);
                jawaban[6] = "d";
            }
        });
    }

    public void setUpSoal8()
    {
        TextView soal = findViewById(R.id.soal8);
        TextView opsi8a = findViewById(R.id.opsi8a);
        TextView opsi8b = findViewById(R.id.opsi8b);
        TextView opsi8c = findViewById(R.id.opsi8c);
        TextView opsi8d = findViewById(R.id.opsi8d);
        final RadioButton radio8a = findViewById(R.id.radio8a);
        final RadioButton radio8b = findViewById(R.id.radio8b);
        final RadioButton radio8c = findViewById(R.id.radio8c);
        final RadioButton radio8d = findViewById(R.id.radio8d);

        soal.setText(""+soalku[7]);
        opsi8a.setText(""+opsiA[7]);
        opsi8b.setText(""+opsiB[7]);
        opsi8c.setText(""+opsiC[7]);
        opsi8d.setText(""+opsiD[7]);

        radio8a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio8a.setChecked(true);
                radio8b.setChecked(false);
                radio8c.setChecked(false);
                radio8d.setChecked(false);
                jawaban[7] = "a";
            }
        });
        radio8b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio8a.setChecked(false);
                radio8b.setChecked(true);
                radio8c.setChecked(false);
                radio8d.setChecked(false);
                jawaban[7] = "b";
            }
        });
        radio8c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio8a.setChecked(false);
                radio8b.setChecked(false);
                radio8c.setChecked(true);
                radio8d.setChecked(false);
                jawaban[7] = "c";
            }
        });
        radio8d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio8a.setChecked(false);
                radio8b.setChecked(false);
                radio8c.setChecked(false);
                radio8d.setChecked(true);
                jawaban[7] = "d";
            }
        });
    }

    public void setUpSoal9()
    {
        TextView soal = findViewById(R.id.soal9);
        TextView opsi9a = findViewById(R.id.opsi9a);
        TextView opsi9b = findViewById(R.id.opsi9b);
        TextView opsi9c = findViewById(R.id.opsi9c);
        TextView opsi9d = findViewById(R.id.opsi9d);
        final RadioButton radio9a = findViewById(R.id.radio9a);
        final RadioButton radio9b = findViewById(R.id.radio9b);
        final RadioButton radio9c = findViewById(R.id.radio9c);
        final RadioButton radio9d = findViewById(R.id.radio9d);

        soal.setText(""+soalku[8]);
        opsi9a.setText(""+opsiA[8]);
        opsi9b.setText(""+opsiB[8]);
        opsi9c.setText(""+opsiC[8]);
        opsi9d.setText(""+opsiD[8]);

        radio9a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio9a.setChecked(true);
                radio9b.setChecked(false);
                radio9c.setChecked(false);
                radio9d.setChecked(false);
                jawaban[8] = "a";
            }
        });
        radio9b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio9a.setChecked(false);
                radio9b.setChecked(true);
                radio9c.setChecked(false);
                radio9d.setChecked(false);
                jawaban[8] = "b";
            }
        });
        radio9c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio9a.setChecked(false);
                radio9b.setChecked(false);
                radio9c.setChecked(true);
                radio9d.setChecked(false);
                jawaban[8] = "c";
            }
        });
        radio9d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio9a.setChecked(false);
                radio9b.setChecked(false);
                radio9c.setChecked(false);
                radio9d.setChecked(true);
                jawaban[8] = "d";
            }
        });
    }

    public void setUpSoal10()
    {
        TextView soal = findViewById(R.id.soal10);
        TextView opsi10a = findViewById(R.id.opsi10a);
        TextView opsi10b = findViewById(R.id.opsi10b);
        TextView opsi10c = findViewById(R.id.opsi10c);
        TextView opsi10d = findViewById(R.id.opsi10d);
        final RadioButton radio10a = findViewById(R.id.radio10a);
        final RadioButton radio10b = findViewById(R.id.radio10b);
        final RadioButton radio10c = findViewById(R.id.radio10c);
        final RadioButton radio10d = findViewById(R.id.radio10d);

        soal.setText(""+soalku[9]);
        opsi10a.setText(""+opsiA[9]);
        opsi10b.setText(""+opsiB[9]);
        opsi10c.setText(""+opsiC[9]);
        opsi10d.setText(""+opsiD[9]);

        radio10a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio10a.setChecked(true);
                radio10b.setChecked(false);
                radio10c.setChecked(false);
                radio10d.setChecked(false);
                jawaban[9] = "a";
            }
        });
        radio10b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio10a.setChecked(false);
                radio10b.setChecked(true);
                radio10c.setChecked(false);
                radio10d.setChecked(false);
                jawaban[9] = "b";
            }
        });
        radio10c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio10a.setChecked(false);
                radio10b.setChecked(false);
                radio10c.setChecked(true);
                radio10d.setChecked(false);
                jawaban[9] = "c";
            }
        });
        radio10d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio10a.setChecked(false);
                radio10b.setChecked(false);
                radio10c.setChecked(false);
                radio10d.setChecked(true);
                jawaban[9] = "d";
            }
        });
    }

    public void setUpSoal11()
    {
        TextView soal = findViewById(R.id.soal11);
        TextView opsi11a = findViewById(R.id.opsi11a);
        TextView opsi11b = findViewById(R.id.opsi11b);
        TextView opsi11c = findViewById(R.id.opsi11c);
        TextView opsi11d = findViewById(R.id.opsi11d);
        final RadioButton radio11a = findViewById(R.id.radio11a);
        final RadioButton radio11b = findViewById(R.id.radio11b);
        final RadioButton radio11c = findViewById(R.id.radio11c);
        final RadioButton radio11d = findViewById(R.id.radio11d);

        soal.setText(""+soalku[10]);
        opsi11a.setText(""+opsiA[10]);
        opsi11b.setText(""+opsiB[10]);
        opsi11c.setText(""+opsiC[10]);
        opsi11d.setText(""+opsiD[10]);

        radio11a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio11a.setChecked(true);
                radio11b.setChecked(false);
                radio11c.setChecked(false);
                radio11d.setChecked(false);
                jawaban[10] = "a";
            }
        });
        radio11b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio11a.setChecked(false);
                radio11b.setChecked(true);
                radio11c.setChecked(false);
                radio11d.setChecked(false);
                jawaban[10] = "b";
            }
        });
        radio11c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio11a.setChecked(false);
                radio11b.setChecked(false);
                radio11c.setChecked(true);
                radio11d.setChecked(false);
                jawaban[10] = "c";
            }
        });
        radio11d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio11a.setChecked(false);
                radio11b.setChecked(false);
                radio11c.setChecked(false);
                radio11d.setChecked(true);
                jawaban[10] = "d";
            }
        });
    }

    public void setUpSoal12()
    {
        TextView soal = findViewById(R.id.soal12);
        TextView opsi12a = findViewById(R.id.opsi12a);
        TextView opsi12b = findViewById(R.id.opsi12b);
        TextView opsi12c = findViewById(R.id.opsi12c);
        TextView opsi12d = findViewById(R.id.opsi12d);
        final RadioButton radio12a = findViewById(R.id.radio12a);
        final RadioButton radio12b = findViewById(R.id.radio12b);
        final RadioButton radio12c = findViewById(R.id.radio12c);
        final RadioButton radio12d = findViewById(R.id.radio12d);

        soal.setText(""+soalku[11]);
        opsi12a.setText(""+opsiA[11]);
        opsi12b.setText(""+opsiB[11]);
        opsi12c.setText(""+opsiC[11]);
        opsi12d.setText(""+opsiD[11]);

        radio12a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio12a.setChecked(true);
                radio12b.setChecked(false);
                radio12c.setChecked(false);
                radio12d.setChecked(false);
                jawaban[11] = "a";
            }
        });
        radio12b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio12a.setChecked(false);
                radio12b.setChecked(true);
                radio12c.setChecked(false);
                radio12d.setChecked(false);
                jawaban[11] = "b";
            }
        });
        radio12c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio12a.setChecked(false);
                radio12b.setChecked(false);
                radio12c.setChecked(true);
                radio12d.setChecked(false);
                jawaban[11] = "c";
            }
        });
        radio12d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio12a.setChecked(false);
                radio12b.setChecked(false);
                radio12c.setChecked(false);
                radio12d.setChecked(true);
                jawaban[11] = "d";
            }
        });
    }

    public void setUpSoal13()
    {
        TextView soal = findViewById(R.id.soal13);
        TextView opsi13a = findViewById(R.id.opsi13a);
        TextView opsi13b = findViewById(R.id.opsi13b);
        TextView opsi13c = findViewById(R.id.opsi13c);
        TextView opsi13d = findViewById(R.id.opsi13d);
        final RadioButton radio13a = findViewById(R.id.radio13a);
        final RadioButton radio13b = findViewById(R.id.radio13b);
        final RadioButton radio13c = findViewById(R.id.radio13c);
        final RadioButton radio13d = findViewById(R.id.radio13d);

        soal.setText(""+soalku[12]);
        opsi13a.setText(""+opsiA[12]);
        opsi13b.setText(""+opsiB[12]);
        opsi13c.setText(""+opsiC[12]);
        opsi13d.setText(""+opsiD[12]);

        radio13a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio13a.setChecked(true);
                radio13b.setChecked(false);
                radio13c.setChecked(false);
                radio13d.setChecked(false);
                jawaban[12] = "a";
            }
        });
        radio13b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio13a.setChecked(false);
                radio13b.setChecked(true);
                radio13c.setChecked(false);
                radio13d.setChecked(false);
                jawaban[12] = "b";
            }
        });
        radio13c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio13a.setChecked(false);
                radio13b.setChecked(false);
                radio13c.setChecked(true);
                radio13d.setChecked(false);
                jawaban[12] = "c";
            }
        });
        radio13d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio13a.setChecked(false);
                radio13b.setChecked(false);
                radio13c.setChecked(false);
                radio13d.setChecked(true);
                jawaban[12] = "d";
            }
        });
    }

    public void setUpSoal14()
    {
        TextView soal = findViewById(R.id.soal14);
        TextView opsi14a = findViewById(R.id.opsi14a);
        TextView opsi14b = findViewById(R.id.opsi14b);
        TextView opsi14c = findViewById(R.id.opsi14c);
        TextView opsi14d = findViewById(R.id.opsi14d);
        final RadioButton radio14a = findViewById(R.id.radio14a);
        final RadioButton radio14b = findViewById(R.id.radio14b);
        final RadioButton radio14c = findViewById(R.id.radio14c);
        final RadioButton radio14d = findViewById(R.id.radio14d);

        soal.setText(""+soalku[13]);
        opsi14a.setText(""+opsiA[13]);
        opsi14b.setText(""+opsiB[13]);
        opsi14c.setText(""+opsiC[13]);
        opsi14d.setText(""+opsiD[13]);

        radio14a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio14a.setChecked(true);
                radio14b.setChecked(false);
                radio14c.setChecked(false);
                radio14d.setChecked(false);
                jawaban[13] = "a";
            }
        });
        radio14b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio14a.setChecked(false);
                radio14b.setChecked(true);
                radio14c.setChecked(false);
                radio14d.setChecked(false);
                jawaban[13] = "b";
            }
        });
        radio14c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio14a.setChecked(false);
                radio14b.setChecked(false);
                radio14c.setChecked(true);
                radio14d.setChecked(false);
                jawaban[13] = "c";
            }
        });
        radio14d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio14a.setChecked(false);
                radio14b.setChecked(false);
                radio14c.setChecked(false);
                radio14d.setChecked(true);
                jawaban[13] = "d";
            }
        });
    }

    public void setUpSoal15()
    {
        TextView soal = findViewById(R.id.soal15);
        TextView opsi15a = findViewById(R.id.opsi15a);
        TextView opsi15b = findViewById(R.id.opsi15b);
        TextView opsi15c = findViewById(R.id.opsi15c);
        TextView opsi15d = findViewById(R.id.opsi15d);
        final RadioButton radio15a = findViewById(R.id.radio15a);
        final RadioButton radio15b = findViewById(R.id.radio15b);
        final RadioButton radio15c = findViewById(R.id.radio15c);
        final RadioButton radio15d = findViewById(R.id.radio15d);

        soal.setText(""+soalku[14]);
        opsi15a.setText(""+opsiA[14]);
        opsi15b.setText(""+opsiB[14]);
        opsi15c.setText(""+opsiC[14]);
        opsi15d.setText(""+opsiD[14]);

        radio15a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio15a.setChecked(true);
                radio15b.setChecked(false);
                radio15c.setChecked(false);
                radio15d.setChecked(false);
                jawaban[14] = "a";
            }
        });
        radio15b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio15a.setChecked(false);
                radio15b.setChecked(true);
                radio15c.setChecked(false);
                radio15d.setChecked(false);
                jawaban[14] = "b";
            }
        });
        radio15c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio15a.setChecked(false);
                radio15b.setChecked(false);
                radio15c.setChecked(true);
                radio15d.setChecked(false);
                jawaban[14] = "c";
            }
        });
        radio15d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio15a.setChecked(false);
                radio15b.setChecked(false);
                radio15c.setChecked(false);
                radio15d.setChecked(true);
                jawaban[14] = "d";
            }
        });
    }

    public void setUpSoal16()
    {
        TextView soal = findViewById(R.id.soal16);
        TextView opsi16a = findViewById(R.id.opsi16a);
        TextView opsi16b = findViewById(R.id.opsi16b);
        TextView opsi16c = findViewById(R.id.opsi16c);
        TextView opsi16d = findViewById(R.id.opsi16d);
        final RadioButton radio16a = findViewById(R.id.radio16a);
        final RadioButton radio16b = findViewById(R.id.radio16b);
        final RadioButton radio16c = findViewById(R.id.radio16c);
        final RadioButton radio16d = findViewById(R.id.radio16d);

        soal.setText(""+soalku[15]);
        opsi16a.setText(""+opsiA[15]);
        opsi16b.setText(""+opsiB[15]);
        opsi16c.setText(""+opsiC[15]);
        opsi16d.setText(""+opsiD[15]);

        radio16a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio16a.setChecked(true);
                radio16b.setChecked(false);
                radio16c.setChecked(false);
                radio16d.setChecked(false);
                jawaban[15] = "a";
            }
        });
        radio16b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio16a.setChecked(false);
                radio16b.setChecked(true);
                radio16c.setChecked(false);
                radio16d.setChecked(false);
                jawaban[15] = "b";
            }
        });
        radio16c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio16a.setChecked(false);
                radio16b.setChecked(false);
                radio16c.setChecked(true);
                radio16d.setChecked(false);
                jawaban[15] = "c";
            }
        });
        radio16d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio16a.setChecked(false);
                radio16b.setChecked(false);
                radio16c.setChecked(false);
                radio16d.setChecked(true);
                jawaban[15] = "d";
            }
        });
    }

    public void setUpSoal17()
    {
        TextView soal = findViewById(R.id.soal17);
        TextView opsi17a = findViewById(R.id.opsi17a);
        TextView opsi17b = findViewById(R.id.opsi17b);
        TextView opsi17c = findViewById(R.id.opsi17c);
        TextView opsi17d = findViewById(R.id.opsi17d);
        final RadioButton radio17a = findViewById(R.id.radio17a);
        final RadioButton radio17b = findViewById(R.id.radio17b);
        final RadioButton radio17c = findViewById(R.id.radio17c);
        final RadioButton radio17d = findViewById(R.id.radio17d);

        soal.setText(""+soalku[16]);
        opsi17a.setText(""+opsiA[16]);
        opsi17b.setText(""+opsiB[16]);
        opsi17c.setText(""+opsiC[16]);
        opsi17d.setText(""+opsiD[16]);

        radio17a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio17a.setChecked(true);
                radio17b.setChecked(false);
                radio17c.setChecked(false);
                radio17d.setChecked(false);
                jawaban[16] = "a";
            }
        });
        radio17b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio17a.setChecked(false);
                radio17b.setChecked(true);
                radio17c.setChecked(false);
                radio17d.setChecked(false);
                jawaban[16] = "b";
            }
        });
        radio17c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio17a.setChecked(false);
                radio17b.setChecked(false);
                radio17c.setChecked(true);
                radio17d.setChecked(false);
                jawaban[16] = "c";
            }
        });
        radio17d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio17a.setChecked(false);
                radio17b.setChecked(false);
                radio17c.setChecked(false);
                radio17d.setChecked(true);
                jawaban[16] = "d";
            }
        });
    }

    public void setUpSoal18()
    {
        TextView soal = findViewById(R.id.soal18);
        TextView opsi18a = findViewById(R.id.opsi18a);
        TextView opsi18b = findViewById(R.id.opsi18b);
        TextView opsi18c = findViewById(R.id.opsi18c);
        TextView opsi18d = findViewById(R.id.opsi18d);
        final RadioButton radio18a = findViewById(R.id.radio18a);
        final RadioButton radio18b = findViewById(R.id.radio18b);
        final RadioButton radio18c = findViewById(R.id.radio18c);
        final RadioButton radio18d = findViewById(R.id.radio18d);

        soal.setText(""+soalku[17]);
        opsi18a.setText(""+opsiA[17]);
        opsi18b.setText(""+opsiB[17]);
        opsi18c.setText(""+opsiC[17]);
        opsi18d.setText(""+opsiD[17]);

        radio18a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio18a.setChecked(true);
                radio18b.setChecked(false);
                radio18c.setChecked(false);
                radio18d.setChecked(false);
                jawaban[17] = "a";
            }
        });
        radio18b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio18a.setChecked(false);
                radio18b.setChecked(true);
                radio18c.setChecked(false);
                radio18d.setChecked(false);
                jawaban[17] = "b";
            }
        });
        radio18c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio18a.setChecked(false);
                radio18b.setChecked(false);
                radio18c.setChecked(true);
                radio18d.setChecked(false);
                jawaban[17] = "c";
            }
        });
        radio18d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio18a.setChecked(false);
                radio18b.setChecked(false);
                radio18c.setChecked(false);
                radio18d.setChecked(true);
                jawaban[17] = "d";
            }
        });
    }

    public void setUpSoal19()
    {
        TextView soal = findViewById(R.id.soal19);
        TextView opsi19a = findViewById(R.id.opsi19a);
        TextView opsi19b = findViewById(R.id.opsi19b);
        TextView opsi19c = findViewById(R.id.opsi19c);
        TextView opsi19d = findViewById(R.id.opsi19d);
        final RadioButton radio19a = findViewById(R.id.radio19a);
        final RadioButton radio19b = findViewById(R.id.radio19b);
        final RadioButton radio19c = findViewById(R.id.radio19c);
        final RadioButton radio19d = findViewById(R.id.radio19d);

        soal.setText(""+soalku[18]);
        opsi19a.setText(""+opsiA[18]);
        opsi19b.setText(""+opsiB[18]);
        opsi19c.setText(""+opsiC[18]);
        opsi19d.setText(""+opsiD[18]);

        radio19a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio19a.setChecked(true);
                radio19b.setChecked(false);
                radio19c.setChecked(false);
                radio19d.setChecked(false);
                jawaban[18] = "a";
            }
        });
        radio19b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio19a.setChecked(false);
                radio19b.setChecked(true);
                radio19c.setChecked(false);
                radio19d.setChecked(false);
                jawaban[18] = "b";
            }
        });
        radio19c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio19a.setChecked(false);
                radio19b.setChecked(false);
                radio19c.setChecked(true);
                radio19d.setChecked(false);
                jawaban[18] = "c";
            }
        });
        radio19d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio19a.setChecked(false);
                radio19b.setChecked(false);
                radio19c.setChecked(false);
                radio19d.setChecked(true);
                jawaban[18] = "d";
            }
        });
    }

    public void setUpSoal20()
    {
        final ZoomImageView zoomImage2 = findViewById(R.id.zoom_image2);
        Drawable img2 = getResources().getDrawable( R.drawable.soalgb2);
        zoomImage2.setImageDrawable(img2);

        TextView soal = findViewById(R.id.soal20);
        TextView opsi20a = findViewById(R.id.opsi20a);
        TextView opsi20b = findViewById(R.id.opsi20b);
        TextView opsi20c = findViewById(R.id.opsi20c);
        TextView opsi20d = findViewById(R.id.opsi20d);
        final RadioButton radio20a = findViewById(R.id.radio20a);
        final RadioButton radio20b = findViewById(R.id.radio20b);
        final RadioButton radio20c = findViewById(R.id.radio20c);
        final RadioButton radio20d = findViewById(R.id.radio20d);

        soal.setText(""+soalku[19]);
        opsi20a.setText(""+opsiA[19]);
        opsi20b.setText(""+opsiB[19]);
        opsi20c.setText(""+opsiC[19]);
        opsi20d.setText(""+opsiD[19]);

        radio20a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio20a.setChecked(true);
                radio20b.setChecked(false);
                radio20c.setChecked(false);
                radio20d.setChecked(false);
                jawaban[19] = "a";
            }
        });
        radio20b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio20a.setChecked(false);
                radio20b.setChecked(true);
                radio20c.setChecked(false);
                radio20d.setChecked(false);
                jawaban[19] = "b";
            }
        });
        radio20c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio20a.setChecked(false);
                radio20b.setChecked(false);
                radio20c.setChecked(true);
                radio20d.setChecked(false);
                jawaban[19] = "c";
            }
        });
        radio20d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio20a.setChecked(false);
                radio20b.setChecked(false);
                radio20c.setChecked(false);
                radio20d.setChecked(true);
                jawaban[19] = "d";
            }
        });
    }

    public void setUpSoal21()
    {
        TextView soal = findViewById(R.id.soal21);
        final RadioButton radio21a = findViewById(R.id.radio21a);
        final RadioButton radio21b = findViewById(R.id.radio21b);
        final RadioButton radio21c = findViewById(R.id.radio21c);
        final RadioButton radio21d = findViewById(R.id.radio21d);

        soal.setText(""+soalku[20]);

        radio21a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio21a.setChecked(true);
                radio21b.setChecked(false);
                radio21c.setChecked(false);
                radio21d.setChecked(false);
                jawaban[20] = "a";
            }
        });
        radio21b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio21a.setChecked(false);
                radio21b.setChecked(true);
                radio21c.setChecked(false);
                radio21d.setChecked(false);
                jawaban[20] = "b";
            }
        });
        radio21c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio21a.setChecked(false);
                radio21b.setChecked(false);
                radio21c.setChecked(true);
                radio21d.setChecked(false);
                jawaban[20] = "c";
            }
        });
        radio21d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio21a.setChecked(false);
                radio21b.setChecked(false);
                radio21c.setChecked(false);
                radio21d.setChecked(true);
                jawaban[20] = "d";
            }
        });
    }

    public void setUpSoal22()
    {
        TextView soal = findViewById(R.id.soal22);
        TextView opsi22a = findViewById(R.id.opsi22a);
        TextView opsi22b = findViewById(R.id.opsi22b);
        TextView opsi22c = findViewById(R.id.opsi22c);
        TextView opsi22d = findViewById(R.id.opsi22d);
        final RadioButton radio22a = findViewById(R.id.radio22a);
        final RadioButton radio22b = findViewById(R.id.radio22b);
        final RadioButton radio22c = findViewById(R.id.radio22c);
        final RadioButton radio22d = findViewById(R.id.radio22d);

        soal.setText(""+soalku[21]);
        opsi22a.setText(""+opsiA[21]);
        opsi22b.setText(""+opsiB[21]);
        opsi22c.setText(""+opsiC[21]);
        opsi22d.setText(""+opsiD[21]);

        radio22a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio22a.setChecked(true);
                radio22b.setChecked(false);
                radio22c.setChecked(false);
                radio22d.setChecked(false);
                jawaban[21] = "a";
            }
        });
        radio22b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio22a.setChecked(false);
                radio22b.setChecked(true);
                radio22c.setChecked(false);
                radio22d.setChecked(false);
                jawaban[21] = "b";
            }
        });
        radio22c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio22a.setChecked(false);
                radio22b.setChecked(false);
                radio22c.setChecked(true);
                radio22d.setChecked(false);
                jawaban[21] = "c";
            }
        });
        radio22d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio22a.setChecked(false);
                radio22b.setChecked(false);
                radio22c.setChecked(false);
                radio22d.setChecked(true);
                jawaban[21] = "d";
            }
        });
    }

    public void setUpSoal23()
    {
        final ZoomImageView zoomImage3 = findViewById(R.id.zoom_image3);
        Drawable img3 = getResources().getDrawable( R.drawable.soalgb3);
        zoomImage3.setImageDrawable(img3);

        TextView soal = findViewById(R.id.soal23);
        TextView opsi23a = findViewById(R.id.opsi23a);
        TextView opsi23b = findViewById(R.id.opsi23b);
        TextView opsi23c = findViewById(R.id.opsi23c);
        TextView opsi23d = findViewById(R.id.opsi23d);
        final RadioButton radio23a = findViewById(R.id.radio23a);
        final RadioButton radio23b = findViewById(R.id.radio23b);
        final RadioButton radio23c = findViewById(R.id.radio23c);
        final RadioButton radio23d = findViewById(R.id.radio23d);

        soal.setText(""+soalku[22]);
        opsi23a.setText(""+opsiA[22]);
        opsi23b.setText(""+opsiB[22]);
        opsi23c.setText(""+opsiC[22]);
        opsi23d.setText(""+opsiD[22]);

        radio23a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio23a.setChecked(true);
                radio23b.setChecked(false);
                radio23c.setChecked(false);
                radio23d.setChecked(false);
                jawaban[22] = "a";
            }
        });
        radio23b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio23a.setChecked(false);
                radio23b.setChecked(true);
                radio23c.setChecked(false);
                radio23d.setChecked(false);
                jawaban[22] = "b";
            }
        });
        radio23c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio23a.setChecked(false);
                radio23b.setChecked(false);
                radio23c.setChecked(true);
                radio23d.setChecked(false);
                jawaban[22] = "c";
            }
        });
        radio23d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio23a.setChecked(false);
                radio23b.setChecked(false);
                radio23c.setChecked(false);
                radio23d.setChecked(true);
                jawaban[22] = "d";
            }
        });
    }

    public void setUpSoal24()
    {
        TextView soal = findViewById(R.id.soal24);
        TextView opsi24a = findViewById(R.id.opsi24a);
        TextView opsi24b = findViewById(R.id.opsi24b);
        TextView opsi24c = findViewById(R.id.opsi24c);
        TextView opsi24d = findViewById(R.id.opsi24d);
        final RadioButton radio24a = findViewById(R.id.radio24a);
        final RadioButton radio24b = findViewById(R.id.radio24b);
        final RadioButton radio24c = findViewById(R.id.radio24c);
        final RadioButton radio24d = findViewById(R.id.radio24d);

        soal.setText(""+soalku[23]);
        opsi24a.setText(""+opsiA[23]);
        opsi24b.setText(""+opsiB[23]);
        opsi24c.setText(""+opsiC[23]);
        opsi24d.setText(""+opsiD[23]);

        radio24a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio24a.setChecked(true);
                radio24b.setChecked(false);
                radio24c.setChecked(false);
                radio24d.setChecked(false);
                jawaban[23] = "a";
            }
        });
        radio24b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio24a.setChecked(false);
                radio24b.setChecked(true);
                radio24c.setChecked(false);
                radio24d.setChecked(false);
                jawaban[23] = "b";
            }
        });
        radio24c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio24a.setChecked(false);
                radio24b.setChecked(false);
                radio24c.setChecked(true);
                radio24d.setChecked(false);
                jawaban[23] = "c";
            }
        });
        radio24d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio24a.setChecked(false);
                radio24b.setChecked(false);
                radio24c.setChecked(false);
                radio24d.setChecked(true);
                jawaban[23] = "d";
            }
        });
    }

    public void setUpSoal25()
    {
        TextView soal = findViewById(R.id.soal25);
        TextView opsi25a = findViewById(R.id.opsi25a);
        TextView opsi25b = findViewById(R.id.opsi25b);
        TextView opsi25c = findViewById(R.id.opsi25c);
        TextView opsi25d = findViewById(R.id.opsi25d);
        final RadioButton radio25a = findViewById(R.id.radio25a);
        final RadioButton radio25b = findViewById(R.id.radio25b);
        final RadioButton radio25c = findViewById(R.id.radio25c);
        final RadioButton radio25d = findViewById(R.id.radio25d);

        soal.setText(""+soalku[24]);
        opsi25a.setText(""+opsiA[24]);
        opsi25b.setText(""+opsiB[24]);
        opsi25c.setText(""+opsiC[24]);
        opsi25d.setText(""+opsiD[24]);

        radio25a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio25a.setChecked(true);
                radio25b.setChecked(false);
                radio25c.setChecked(false);
                radio25d.setChecked(false);
                jawaban[24] = "a";
            }
        });
        radio25b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio25a.setChecked(false);
                radio25b.setChecked(true);
                radio25c.setChecked(false);
                radio25d.setChecked(false);
                jawaban[24] = "b";
            }
        });
        radio25c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio25a.setChecked(false);
                radio25b.setChecked(false);
                radio25c.setChecked(true);
                radio25d.setChecked(false);
                jawaban[24] = "c";
            }
        });
        radio25d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio25a.setChecked(false);
                radio25b.setChecked(false);
                radio25c.setChecked(false);
                radio25d.setChecked(true);
                jawaban[24] = "d";
            }
        });
    }

    public void setUpSoal26()
    {
        final ZoomImageView zoomImage4 = findViewById(R.id.zoom_image4);
        Drawable img4 = getResources().getDrawable( R.drawable.soalgb4);
        zoomImage4.setImageDrawable(img4);

        TextView soal = findViewById(R.id.soal26);
        TextView opsi26a = findViewById(R.id.opsi26a);
        TextView opsi26b = findViewById(R.id.opsi26b);
        TextView opsi26c = findViewById(R.id.opsi26c);
        TextView opsi26d = findViewById(R.id.opsi26d);
        final RadioButton radio26a = findViewById(R.id.radio26a);
        final RadioButton radio26b = findViewById(R.id.radio26b);
        final RadioButton radio26c = findViewById(R.id.radio26c);
        final RadioButton radio26d = findViewById(R.id.radio26d);

        soal.setText(""+soalku[25]);
        opsi26a.setText(""+opsiA[25]);
        opsi26b.setText(""+opsiB[25]);
        opsi26c.setText(""+opsiC[25]);
        opsi26d.setText(""+opsiD[25]);

        radio26a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio26a.setChecked(true);
                radio26b.setChecked(false);
                radio26c.setChecked(false);
                radio26d.setChecked(false);
                jawaban[25] = "a";
            }
        });
        radio26b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio26a.setChecked(false);
                radio26b.setChecked(true);
                radio26c.setChecked(false);
                radio26d.setChecked(false);
                jawaban[25] = "b";
            }
        });
        radio26c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio26a.setChecked(false);
                radio26b.setChecked(false);
                radio26c.setChecked(true);
                radio26d.setChecked(false);
                jawaban[25] = "c";
            }
        });
        radio26d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio26a.setChecked(false);
                radio26b.setChecked(false);
                radio26c.setChecked(false);
                radio26d.setChecked(true);
                jawaban[25] = "d";
            }
        });
    }

    public void setUpSoal27()
    {
        TextView soal = findViewById(R.id.soal27);
        TextView opsi27a = findViewById(R.id.opsi27a);
        TextView opsi27b = findViewById(R.id.opsi27b);
        TextView opsi27c = findViewById(R.id.opsi27c);
        TextView opsi27d = findViewById(R.id.opsi27d);
        final RadioButton radio27a = findViewById(R.id.radio27a);
        final RadioButton radio27b = findViewById(R.id.radio27b);
        final RadioButton radio27c = findViewById(R.id.radio27c);
        final RadioButton radio27d = findViewById(R.id.radio27d);

        soal.setText(""+soalku[26]);
        opsi27a.setText(""+opsiA[26]);
        opsi27b.setText(""+opsiB[26]);
        opsi27c.setText(""+opsiC[26]);
        opsi27d.setText(""+opsiD[26]);

        radio27a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio27a.setChecked(true);
                radio27b.setChecked(false);
                radio27c.setChecked(false);
                radio27d.setChecked(false);
                jawaban[26] = "a";
            }
        });
        radio27b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio27a.setChecked(false);
                radio27b.setChecked(true);
                radio27c.setChecked(false);
                radio27d.setChecked(false);
                jawaban[26] = "b";
            }
        });
        radio27c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio27a.setChecked(false);
                radio27b.setChecked(false);
                radio27c.setChecked(true);
                radio27d.setChecked(false);
                jawaban[26] = "c";
            }
        });
        radio27d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio27a.setChecked(false);
                radio27b.setChecked(false);
                radio27c.setChecked(false);
                radio27d.setChecked(true);
                jawaban[26] = "d";
            }
        });
    }

    public void setUpSoal28()
    {
        TextView soal = findViewById(R.id.soal28);
        TextView opsi28a = findViewById(R.id.opsi28a);
        TextView opsi28b = findViewById(R.id.opsi28b);
        TextView opsi28c = findViewById(R.id.opsi28c);
        TextView opsi28d = findViewById(R.id.opsi28d);
        final RadioButton radio28a = findViewById(R.id.radio28a);
        final RadioButton radio28b = findViewById(R.id.radio28b);
        final RadioButton radio28c = findViewById(R.id.radio28c);
        final RadioButton radio28d = findViewById(R.id.radio28d);

        soal.setText(""+soalku[27]);
        opsi28a.setText(""+opsiA[27]);
        opsi28b.setText(""+opsiB[27]);
        opsi28c.setText(""+opsiC[27]);
        opsi28d.setText(""+opsiD[27]);

        radio28a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio28a.setChecked(true);
                radio28b.setChecked(false);
                radio28c.setChecked(false);
                radio28d.setChecked(false);
                jawaban[27] = "a";
            }
        });
        radio28b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio28a.setChecked(false);
                radio28b.setChecked(true);
                radio28c.setChecked(false);
                radio28d.setChecked(false);
                jawaban[27] = "b";
            }
        });
        radio28c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio28a.setChecked(false);
                radio28b.setChecked(false);
                radio28c.setChecked(true);
                radio28d.setChecked(false);
                jawaban[27] = "c";
            }
        });
        radio28d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio28a.setChecked(false);
                radio28b.setChecked(false);
                radio28c.setChecked(false);
                radio28d.setChecked(true);
                jawaban[27] = "d";
            }
        });
    }

    public void setUpSoal29()
    {
        final ZoomImageView zoomImage5 = findViewById(R.id.zoom_image5);
        Drawable img5 = getResources().getDrawable( R.drawable.soalgb5);
        zoomImage5.setImageDrawable(img5);

        TextView soal = findViewById(R.id.soal29);
        TextView opsi29a = findViewById(R.id.opsi29a);
        TextView opsi29b = findViewById(R.id.opsi29b);
        TextView opsi29c = findViewById(R.id.opsi29c);
        TextView opsi29d = findViewById(R.id.opsi29d);
        final RadioButton radio29a = findViewById(R.id.radio29a);
        final RadioButton radio29b = findViewById(R.id.radio29b);
        final RadioButton radio29c = findViewById(R.id.radio29c);
        final RadioButton radio29d = findViewById(R.id.radio29d);

        soal.setText(""+soalku[28]);
        opsi29a.setText(""+opsiA[28]);
        opsi29b.setText(""+opsiB[28]);
        opsi29c.setText(""+opsiC[28]);
        opsi29d.setText(""+opsiD[28]);

        radio29a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio29a.setChecked(true);
                radio29b.setChecked(false);
                radio29c.setChecked(false);
                radio29d.setChecked(false);
                jawaban[28] = "a";
            }
        });
        radio29b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio29a.setChecked(false);
                radio29b.setChecked(true);
                radio29c.setChecked(false);
                radio29d.setChecked(false);
                jawaban[28] = "b";
            }
        });
        radio29c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio29a.setChecked(false);
                radio29b.setChecked(false);
                radio29c.setChecked(true);
                radio29d.setChecked(false);
                jawaban[28] = "c";
            }
        });
        radio29d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio29a.setChecked(false);
                radio29b.setChecked(false);
                radio29c.setChecked(false);
                radio29d.setChecked(true);
                jawaban[28] = "d";
            }
        });
    }

    public void setUpSoal30()
    {
        TextView soal = findViewById(R.id.soal30);
        TextView opsi30a = findViewById(R.id.opsi30a);
        TextView opsi30b = findViewById(R.id.opsi30b);
        TextView opsi30c = findViewById(R.id.opsi30c);
        TextView opsi30d = findViewById(R.id.opsi30d);
        final RadioButton radio30a = findViewById(R.id.radio30a);
        final RadioButton radio30b = findViewById(R.id.radio30b);
        final RadioButton radio30c = findViewById(R.id.radio30c);
        final RadioButton radio30d = findViewById(R.id.radio30d);

        soal.setText(""+soalku[29]);
        opsi30a.setText(""+opsiA[29]);
        opsi30b.setText(""+opsiB[29]);
        opsi30c.setText(""+opsiC[29]);
        opsi30d.setText(""+opsiD[29]);

        radio30a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio30a.setChecked(true);
                radio30b.setChecked(false);
                radio30c.setChecked(false);
                radio30d.setChecked(false);
                jawaban[29] = "a";
            }
        });
        radio30b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio30a.setChecked(false);
                radio30b.setChecked(true);
                radio30c.setChecked(false);
                radio30d.setChecked(false);
                jawaban[29] = "b";
            }
        });
        radio30c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio30a.setChecked(false);
                radio30b.setChecked(false);
                radio30c.setChecked(true);
                radio30d.setChecked(false);
                jawaban[29] = "c";
            }
        });
        radio30d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio30a.setChecked(false);
                radio30b.setChecked(false);
                radio30c.setChecked(false);
                radio30d.setChecked(true);
                jawaban[29] = "d";
            }
        });
    }

    public void setUpSoal31()
    {
        TextView soal = findViewById(R.id.soal31);
        TextView opsi31a = findViewById(R.id.opsi31a);
        TextView opsi31b = findViewById(R.id.opsi31b);
        TextView opsi31c = findViewById(R.id.opsi31c);
        TextView opsi31d = findViewById(R.id.opsi31d);
        final RadioButton radio31a = findViewById(R.id.radio31a);
        final RadioButton radio31b = findViewById(R.id.radio31b);
        final RadioButton radio31c = findViewById(R.id.radio31c);
        final RadioButton radio31d = findViewById(R.id.radio31d);

        soal.setText(""+soalku[30]);
        opsi31a.setText(""+opsiA[30]);
        opsi31b.setText(""+opsiB[30]);
        opsi31c.setText(""+opsiC[30]);
        opsi31d.setText(""+opsiD[30]);

        radio31a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio31a.setChecked(true);
                radio31b.setChecked(false);
                radio31c.setChecked(false);
                radio31d.setChecked(false);
                jawaban[30] = "a";
            }
        });
        radio31b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio31a.setChecked(false);
                radio31b.setChecked(true);
                radio31c.setChecked(false);
                radio31d.setChecked(false);
                jawaban[30] = "b";
            }
        });
        radio31c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio31a.setChecked(false);
                radio31b.setChecked(false);
                radio31c.setChecked(true);
                radio31d.setChecked(false);
                jawaban[30] = "c";
            }
        });
        radio31d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio31a.setChecked(false);
                radio31b.setChecked(false);
                radio31c.setChecked(false);
                radio31d.setChecked(true);
                jawaban[30] = "d";
            }
        });
    }

    public void setUpSoal32()
    {
        final ZoomImageView zoomImage6 = findViewById(R.id.zoom_image6);
        Drawable img6 = getResources().getDrawable( R.drawable.soalgb6);
        zoomImage6.setImageDrawable(img6);

        TextView soal = findViewById(R.id.soal32);
        TextView opsi32a = findViewById(R.id.opsi32a);
        TextView opsi32b = findViewById(R.id.opsi32b);
        TextView opsi32c = findViewById(R.id.opsi32c);
        TextView opsi32d = findViewById(R.id.opsi32d);
        final RadioButton radio32a = findViewById(R.id.radio32a);
        final RadioButton radio32b = findViewById(R.id.radio32b);
        final RadioButton radio32c = findViewById(R.id.radio32c);
        final RadioButton radio32d = findViewById(R.id.radio32d);

        soal.setText(""+soalku[31]);
        opsi32a.setText(""+opsiA[31]);
        opsi32b.setText(""+opsiB[31]);
        opsi32c.setText(""+opsiC[31]);
        opsi32d.setText(""+opsiD[31]);

        radio32a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio32a.setChecked(true);
                radio32b.setChecked(false);
                radio32c.setChecked(false);
                radio32d.setChecked(false);
                jawaban[31] = "a";
            }
        });
        radio32b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio32a.setChecked(false);
                radio32b.setChecked(true);
                radio32c.setChecked(false);
                radio32d.setChecked(false);
                jawaban[31] = "b";
            }
        });
        radio32c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio32a.setChecked(false);
                radio32b.setChecked(false);
                radio32c.setChecked(true);
                radio32d.setChecked(false);
                jawaban[31] = "c";
            }
        });
        radio32d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio32a.setChecked(false);
                radio32b.setChecked(false);
                radio32c.setChecked(false);
                radio32d.setChecked(true);
                jawaban[31] = "d";
            }
        });
    }

    public void setUpSoal33()
    {
        final ZoomImageView zoomImage7 = findViewById(R.id.zoom_image7);
        Drawable img7 = getResources().getDrawable( R.drawable.soalgb7);
        zoomImage7.setImageDrawable(img7);

        TextView soal = findViewById(R.id.soal33);
        TextView opsi33a = findViewById(R.id.opsi33a);
        TextView opsi33b = findViewById(R.id.opsi33b);
        TextView opsi33c = findViewById(R.id.opsi33c);
        TextView opsi33d = findViewById(R.id.opsi33d);
        final RadioButton radio33a = findViewById(R.id.radio33a);
        final RadioButton radio33b = findViewById(R.id.radio33b);
        final RadioButton radio33c = findViewById(R.id.radio33c);
        final RadioButton radio33d = findViewById(R.id.radio33d);

        soal.setText(""+soalku[32]);
        opsi33a.setText(""+opsiA[32]);
        opsi33b.setText(""+opsiB[32]);
        opsi33c.setText(""+opsiC[32]);
        opsi33d.setText(""+opsiD[32]);

        radio33a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio33a.setChecked(true);
                radio33b.setChecked(false);
                radio33c.setChecked(false);
                radio33d.setChecked(false);
                jawaban[32] = "a";
            }
        });
        radio33b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio33a.setChecked(false);
                radio33b.setChecked(true);
                radio33c.setChecked(false);
                radio33d.setChecked(false);
                jawaban[32] = "b";
            }
        });
        radio33c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio33a.setChecked(false);
                radio33b.setChecked(false);
                radio33c.setChecked(true);
                radio33d.setChecked(false);
                jawaban[32] = "c";
            }
        });
        radio33d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio33a.setChecked(false);
                radio33b.setChecked(false);
                radio33c.setChecked(false);
                radio33d.setChecked(true);
                jawaban[32] = "d";
            }
        });
    }

    public void setUpSoal34()
    {
        TextView soal = findViewById(R.id.soal34);
        TextView opsi34a = findViewById(R.id.opsi34a);
        TextView opsi34b = findViewById(R.id.opsi34b);
        TextView opsi34c = findViewById(R.id.opsi34c);
        TextView opsi34d = findViewById(R.id.opsi34d);
        final RadioButton radio34a = findViewById(R.id.radio34a);
        final RadioButton radio34b = findViewById(R.id.radio34b);
        final RadioButton radio34c = findViewById(R.id.radio34c);
        final RadioButton radio34d = findViewById(R.id.radio34d);

        soal.setText(""+soalku[33]);
        opsi34a.setText(""+opsiA[33]);
        opsi34b.setText(""+opsiB[33]);
        opsi34c.setText(""+opsiC[33]);
        opsi34d.setText(""+opsiD[33]);

        radio34a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio34a.setChecked(true);
                radio34b.setChecked(false);
                radio34c.setChecked(false);
                radio34d.setChecked(false);
                jawaban[33] = "a";
            }
        });
        radio34b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio34a.setChecked(false);
                radio34b.setChecked(true);
                radio34c.setChecked(false);
                radio34d.setChecked(false);
                jawaban[33] = "b";
            }
        });
        radio34c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio34a.setChecked(false);
                radio34b.setChecked(false);
                radio34c.setChecked(true);
                radio34d.setChecked(false);
                jawaban[33] = "c";
            }
        });
        radio34d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio34a.setChecked(false);
                radio34b.setChecked(false);
                radio34c.setChecked(false);
                radio34d.setChecked(true);
                jawaban[33] = "d";
            }
        });
    }


    public void dialogTimesUp()
    {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.model_times_up);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);


            Button yes = (Button) dialog.findViewById(R.id.yes);
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    completeAnswer();

                }
            });

            dialog.show();
    }

    public void dialog_selesai()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.model_complete_evaluasi);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);


        Button yes = (Button) dialog.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                countDownTimer.cancel();
                textMinute.setText("00");
                textSecond.setText("00");
                completeAnswer();

            }
        });

        Button no = (Button) dialog.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }


    public void completeAnswer()
    {
        for (int i = 0; i < 34; i++)
        {
            if(kunci[i].equals(""+jawaban[i]))
            {
                jumlah_benar = jumlah_benar+1;
            }
        }

        int skor = ( jumlah_benar + 16 ) * 2;
        dialog_skor(skor);
    }

    public void dialog_skor(int skor)
    {
        try {
            Data_Cache data_cache = Data_Cache.findById(Data_Cache.class,1L);
            int mySkor = Integer.parseInt(""+data_cache.skorku);
            if(skor > mySkor)
            {
                data_cache.skorku = ""+skor;
                data_cache.save();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.model_skor);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        TextView text_skor = dialog.findViewById(R.id.skor);
        text_skor.setText(""+skor);

        RatingBar ratingBar = dialog.findViewById(R.id.ratings);
        if(skor <= 20)
        {
            ratingBar.setRating(1);
        }
        else if (skor >20 && skor <= 40 )
        {
            ratingBar.setRating(2);
        }
        else if (skor >40 && skor <= 60 )
        {
            ratingBar.setRating(3);
        }
        else if (skor >60 && skor <= 80 )
        {
            ratingBar.setRating(4);
        }
        else
        {
            ratingBar.setRating(5);
        }


        Button yes = (Button) dialog.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Intent intent = new Intent(Evaluasi_Soal.this, Utama.class);
                startActivity(intent);

            }
        });

        dialog.show();
    }

}

