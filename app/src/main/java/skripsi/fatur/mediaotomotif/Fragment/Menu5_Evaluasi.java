package skripsi.fatur.mediaotomotif.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import skripsi.fatur.mediaotomotif.Database.Data_Cache;
import skripsi.fatur.mediaotomotif.Detail.Evaluasi_Soal;
import skripsi.fatur.mediaotomotif.R;

public class Menu5_Evaluasi extends Fragment {

    View rootview;
    Button mulaiEvaluasi;

    public Menu5_Evaluasi() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootview == null)
        {
            rootview = inflater.inflate(R.layout.activity_menu5_evaluasi, container, false);
            mulaiEvaluasi = rootview.findViewById(R.id.mulaiEvaluasi);
            mulaiEvaluasi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), Evaluasi_Soal.class);
                    startActivity(intent);
                }
            });

            Data_Cache data_cache = Data_Cache.findById(Data_Cache.class,1L);
            TextView skor = rootview.findViewById(R.id.skor);
            skor.setText("Skor Tertinggi : "+data_cache.skorku);
        }

        return rootview;

    }

}
