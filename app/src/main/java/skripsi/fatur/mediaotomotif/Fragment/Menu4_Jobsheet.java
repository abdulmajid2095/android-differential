package skripsi.fatur.mediaotomotif.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import java.io.File;

import skripsi.fatur.mediaotomotif.R;

public class Menu4_Jobsheet extends Fragment {

    View rootview;
    PDFView pdfView;
    CardView share;
    public Menu4_Jobsheet() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootview == null)
        {
            rootview = inflater.inflate(R.layout.activity_menu4__jobsheet, container, false);
            //Uri uri = Uri.parse("android.resource://"+getActivity().getPackageName()+"/"+R.raw.pdfku);
            //File file1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/raw/pdfku.pdf");
            share = rootview.findViewById(R.id.share);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareFile();
                }
            });
        }
        pdfView = rootview.findViewById(R.id.pdfView);
        pdfView.fromAsset("sheet.pdf")
                .defaultPage(0)
                .enableAnnotationRendering(true)
                .scrollHandle(new DefaultScrollHandle(getActivity()))
                .spacing(10) // in dp
                .load();

        return rootview;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (rootview !=null)
            {
                pdfView.loadPages();
            }

        }
    }

    public void shareFile()
    {
        /*File outputFile = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_DOWNLOADS), "sheet.pdf");
        Uri video = Uri.fromFile(outputFile);*/
        //Uri video = Uri.parse("android.resource://skripsi.fatur.mediaotomotif/raw/pdfku.pdf");
        //Uri video = Uri.parse("file:///android_asset/sheet.pdf");
        //Uri video = Uri.fromFile(new File("//android_asset/sheet.pdf"));

        String packageName = ""+getActivity().getPackageName();
        //Toast.makeText(getActivity(),""+packageName,Toast.LENGTH_SHORT).show();
        //Uri video = Uri.parse(String.format("android.resource://%s/%s/%s",this.getActivity().getPackageName(),"raw","sheet.pdf"));
        Uri uri = Uri.parse("android.resource://skripsi.fatur.mediaotomotif/raw/sheet.pdf");

        Uri path = Uri.parse("file:///android_asset/raw/sheet");

        Intent share = new Intent();
        share.setAction(Intent.ACTION_SEND);
        share.setType("application/pdf");
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Bagikan Jobsheet"));
    }
}
