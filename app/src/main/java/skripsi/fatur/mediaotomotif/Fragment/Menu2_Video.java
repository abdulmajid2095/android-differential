package skripsi.fatur.mediaotomotif.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import skripsi.fatur.mediaotomotif.R;
import skripsi.fatur.mediaotomotif.Detail.Video_Play;

public class Menu2_Video extends Fragment implements View.OnClickListener{

    View rootview;
    CardView video1,video2,video3,video4;

    public Menu2_Video() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootview == null)
        {
            rootview = inflater.inflate(R.layout.activity_menu2__video, container, false);

            video1 = (CardView) rootview.findViewById(R.id.video1);
            video1.setOnClickListener(this);
            video2 = (CardView) rootview.findViewById(R.id.video2);
            video2.setOnClickListener(this);
            video3 = (CardView) rootview.findViewById(R.id.video3);
            video3.setOnClickListener(this);
            video4 = (CardView) rootview.findViewById(R.id.video4);
            video4.setOnClickListener(this);
        }

        return rootview;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.video1:
                Intent intent = new Intent(getActivity(), Video_Play.class);
                intent.putExtra("video","1");
                startActivity(intent);
                break;

            case R.id.video2:
                Intent intent2 = new Intent(getActivity(),Video_Play.class);
                intent2.putExtra("video","2");
                startActivity(intent2);
                break;

            case R.id.video3:
                Intent intent3 = new Intent(getActivity(),Video_Play.class);
                intent3.putExtra("video","3");
                startActivity(intent3);
                break;

            case R.id.video4:
                Intent intent4 = new Intent(getActivity(),Video_Play.class);
                intent4.putExtra("video","4");
                startActivity(intent4);
                break;


        }
    }
}
