package skripsi.fatur.mediaotomotif.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import skripsi.fatur.mediaotomotif.Detail.DaftarPustaka;
import skripsi.fatur.mediaotomotif.Home.CaraKerja;
import skripsi.fatur.mediaotomotif.Home.Konstruksi;
import skripsi.fatur.mediaotomotif.Home.Mekanisme;
import skripsi.fatur.mediaotomotif.Home.Pengertian;
import skripsi.fatur.mediaotomotif.Home.PetaMateri;
import skripsi.fatur.mediaotomotif.Home.PetunjukPenggunaan;
import skripsi.fatur.mediaotomotif.Home.Troubleshooting;
import skripsi.fatur.mediaotomotif.R;

public class Menu3_Home extends Fragment implements View.OnClickListener{

    View rootview;
    RelativeLayout menu1,menu2,menu3,menu4,menu5,menu6, menu7, menu8;

    public Menu3_Home() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootview == null)
        {
            rootview = inflater.inflate(R.layout.activity_menu3_home, container, false);

            menu1 = (RelativeLayout) rootview.findViewById(R.id.menu1);
            menu1.setOnClickListener(this);
            menu2 = (RelativeLayout) rootview.findViewById(R.id.menu2);
            menu2.setOnClickListener(this);
            menu3 = (RelativeLayout) rootview.findViewById(R.id.menu3);
            menu3.setOnClickListener(this);
            menu4 = (RelativeLayout) rootview.findViewById(R.id.menu4);
            menu4.setOnClickListener(this);
            menu5 = (RelativeLayout) rootview.findViewById(R.id.menu5);
            menu5.setOnClickListener(this);
            menu6 = (RelativeLayout) rootview.findViewById(R.id.menu6);
            menu6.setOnClickListener(this);
            menu7 = (RelativeLayout) rootview.findViewById(R.id.menu7);
            menu7.setOnClickListener(this);
            menu8 = (RelativeLayout) rootview.findViewById(R.id.menu8);
            menu8.setOnClickListener(this);
        }

        return rootview;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.menu1:
                YoYo.with(Techniques.SlideOutLeft)
                        .duration(350)
                        .playOn(menu1);
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent = new Intent(getActivity(),Pengertian.class);
                        startActivity(intent);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu1);
                    }
                };
                handler.postDelayed(myRunnable,350);
                break;

            case R.id.menu2:
                YoYo.with(Techniques.SlideOutRight)
                        .duration(350)
                        .playOn(menu2);
                Handler handler2 =  new Handler();
                Runnable myRunnable2 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent2 = new Intent(getActivity(),Konstruksi.class);
                        startActivity(intent2);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu2);
                    }
                };
                handler2.postDelayed(myRunnable2,350);
                break;

            case R.id.menu3:
                YoYo.with(Techniques.SlideOutLeft)
                        .duration(350)
                        .playOn(menu3);
                Handler handler3 =  new Handler();
                Runnable myRunnable3 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent3 = new Intent(getActivity(),Mekanisme.class);
                        startActivity(intent3);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu3);
                    }
                };
                handler3.postDelayed(myRunnable3,350);
                break;

            case R.id.menu4:
                YoYo.with(Techniques.SlideOutRight)
                        .duration(350)
                        .playOn(menu4);
                Handler handler4 =  new Handler();
                Runnable myRunnable4 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent4 = new Intent(getActivity(),CaraKerja.class);
                        startActivity(intent4);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu4);
                    }
                };
                handler4.postDelayed(myRunnable4,350);
                break;

            case R.id.menu5:
                YoYo.with(Techniques.SlideOutLeft)
                        .duration(350)
                        .playOn(menu5);
                Handler handler5 =  new Handler();
                Runnable myRunnable5 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent5 = new Intent(getActivity(),Troubleshooting.class);
                        startActivity(intent5);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu5);
                    }
                };
                handler5.postDelayed(myRunnable5,350);
                break;

            case R.id.menu6:
                YoYo.with(Techniques.SlideOutRight)
                        .duration(350)
                        .playOn(menu6);
                Handler handler6 =  new Handler();
                Runnable myRunnable6 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent6 = new Intent(getActivity(), DaftarPustaka.class);
                        startActivity(intent6);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu6);
                    }
                };
                handler6.postDelayed(myRunnable6,350);
                break;

            case R.id.menu7:
                YoYo.with(Techniques.SlideOutRight)
                        .duration(350)
                        .playOn(menu7);
                Handler handler7 =  new Handler();
                Runnable myRunnable7 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent6 = new Intent(getActivity(), PetaMateri.class);
                        startActivity(intent6);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu7);
                    }
                };
                handler7.postDelayed(myRunnable7,350);
                break;

            case R.id.menu8:
                YoYo.with(Techniques.SlideOutLeft)
                        .duration(350)
                        .playOn(menu8);
                Handler handler8 =  new Handler();
                Runnable myRunnable8 = new Runnable() {
                    public void run() {
                        // do something
                        Intent intent6 = new Intent(getActivity(), PetunjukPenggunaan.class);
                        startActivity(intent6);
                        YoYo.with(Techniques.SlideInRight)
                                .duration(1000)
                                .playOn(menu8);
                    }
                };
                handler8.postDelayed(myRunnable8,350);
                break;


        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (rootview != null) {
                animateMenu();
            }
        }
    }

    public void animateMenu()
    {
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu2);
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu3);
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu4);
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu5);
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu6);
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu7);
        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(menu8);

        CountDownTimer countDownTimer = new CountDownTimer(1800, 200) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                int set = (int) millisUntilFinished/200;
                if(set == 8)
                {
                    YoYo.with(Techniques.SlideInRight)
                            .duration(350)
                            .playOn(menu1);
                }
                if(set == 7)
                {
                    YoYo.with(Techniques.SlideInLeft)
                            .duration(350)
                            .playOn(menu2);
                }

                if(set == 6)
                {
                    YoYo.with(Techniques.SlideInRight)
                            .duration(350)
                            .playOn(menu3);
                }

                if(set == 5)
                {
                    YoYo.with(Techniques.SlideInLeft)
                            .duration(350)
                            .playOn(menu4);
                }

                if(set == 4)
                {
                    YoYo.with(Techniques.SlideInRight)
                            .duration(350)
                            .playOn(menu5);
                }

                if(set == 3)
                {
                    YoYo.with(Techniques.SlideInLeft)
                            .duration(350)
                            .playOn(menu7);
                }

                if(set == 2)
                {
                    YoYo.with(Techniques.SlideInRight)
                            .duration(350)
                            .playOn(menu8);
                }

                if(set == 1)
                {
                    YoYo.with(Techniques.SlideInLeft)
                            .duration(350)
                            .playOn(menu6);
                }

            }

            public void onFinish()
            {

            }
        };
        countDownTimer.start();
    }
}
