package skripsi.fatur.mediaotomotif.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import skripsi.fatur.mediaotomotif.R;

public class Menu1_Profile extends Fragment {

    View rootview;

    public Menu1_Profile() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootview == null)
        {
            rootview = inflater.inflate(R.layout.activity_menu1_profile, container, false);


        }

        return rootview;

    }

}
