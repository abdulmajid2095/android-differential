package skripsi.fatur.mediaotomotif.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import com.otaliastudios.zoom.ZoomImageView;

import me.biubiubiu.justifytext.library.JustifyTextView;
import skripsi.fatur.mediaotomotif.R;

public class Mekanisme extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mekanisme);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        JustifyTextView text1 = findViewById(R.id.teks1);
        text1.setText("       Putaran poros engkol dari mesin melalui transmisi oleh propeller shaft diperkecil  sesuai  tenaga  yang  diteruskan  oleh  drive  pinion  ke  ring  gear, sebaliknya momen bertambah maka arah transmisi berubah terhadap arah semula. Pada differential case terdapat dua buah side gear, sehingga bila differential case berputar, maka poros pinion (pinion shaft) ikut berputar yang menyebabkan roda gigi sisi (side gear) juga berputar. Side gear dihubungkan ke poros roda belakang dan memindahkan tenaga putar ke roda. Putaran poros roda menjadi lebih rendah karena tenaga putar pada propeller shaft telah direduksi oleh drive pinion yang berkaitan dengan ring gear yang kontruksi giginya lebih banyak. Adapun macam- macam bentuk persinggungan gigi pada ring gear dan drive pinion (Setiyawan Heri, 2009: 10).                                          ");

        JustifyTextView text2 = findViewById(R.id.teks2);
        text2.setText("Perkaitan antara drive pinion dengan ring gear segaris. Konstruksi seperti ini mempunyai bentuk gigi yang lurus, sehingga perkaitan kedua gigi terdapat celah. Oleh sebab itu putaran yang dihasilkan tidak lurus dan tipe semacam ini jarang digunakan pada kendaraan.                               ");

        JustifyTextView text3 = findViewById(R.id.teks3);
        text3.setText("Perkaitan antara drive pinion dengan ring gear berhimpit dengan garis pusat ring gear tanpa ada celah antara kedua gigi sehingga bunyi dan getaran yang timbul sangat kecil dan memiliki momen yang sangat kecil. Konstruksi ini biasannya dipasang pada mobil penggerak depan.                                                   ");

        JustifyTextView text4 = findViewById(R.id.teks4);
        text4.setText("Differential sangat penting karena seluruh tenaga penggerak kendaraan terkonsentrasi pada tipe hypoid bevel pinion and gear yang memiliki keuntungan tidak menyebabkan bunyi, untuk itu diperlukan penyetelan kontak gigi dan backlash yang tepat. Perkaitan antara drive pinion dan ring gear terjadi dibawah garis pusat ring gear. Perkaitan keduanya tanpa ada celah karena konstruksinya berbentuk spiral. Beberapa kelebihan dibandingkan tipe yang lain adalah:                                                   ");

        JustifyTextView text5 = findViewById(R.id.teks5);
        text5.setText("Tipe ini mempunyai prinsip kerja seperti menyapu sehingga gesekan yang timbul  lebih  besar,  oleh  karena  itu  diperlukan  pelumas  khusus  dengan viskositas tinggi untuk mencegah gigi menjadi panas.                                                           ");

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
