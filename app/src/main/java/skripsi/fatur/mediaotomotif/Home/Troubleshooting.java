package skripsi.fatur.mediaotomotif.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.otaliastudios.zoom.ZoomImageView;
import com.ramotion.foldingcell.FoldingCell;

import me.biubiubiu.justifytext.library.JustifyTextView;
import skripsi.fatur.mediaotomotif.R;

public class Troubleshooting extends AppCompatActivity {
    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_troubleshooting);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final FoldingCell fc = (FoldingCell) findViewById(R.id.folding_cell);
        // attach click listener to folding cell
        fc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc.toggle(false);
            }
        });

        final FoldingCell fc2 = (FoldingCell) findViewById(R.id.folding_cell2);
        // attach click listener to folding cell
        fc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc2.toggle(false);
            }
        });

        final FoldingCell fc3 = (FoldingCell) findViewById(R.id.folding_cell3);
        // attach click listener to folding cell
        fc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc3.toggle(false);
            }
        });

        final FoldingCell fc4 = (FoldingCell) findViewById(R.id.folding_cell4);
        // attach click listener to folding cell
        fc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc4.toggle(false);
            }
        });


        final FoldingCell fc5 = (FoldingCell) findViewById(R.id.folding_cell5);
        // attach click listener to folding cell
        fc5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc5.toggle(false);
            }
        });

        final FoldingCell fc6 = (FoldingCell) findViewById(R.id.folding_cell6);
        // attach click listener to folding cell
        fc6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc6.toggle(false);
            }
        });

        final FoldingCell fc7 = (FoldingCell) findViewById(R.id.folding_cell7);
        // attach click listener to folding cell
        fc7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc7.toggle(false);
            }
        });

        final FoldingCell fc8 = (FoldingCell) findViewById(R.id.folding_cell8);
        // attach click listener to folding cell
        fc8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc8.toggle(false);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
