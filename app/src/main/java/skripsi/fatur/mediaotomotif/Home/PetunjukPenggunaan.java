package skripsi.fatur.mediaotomotif.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.otaliastudios.zoom.ZoomImageView;

import skripsi.fatur.mediaotomotif.R;

public class PetunjukPenggunaan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_petunjuk_penggunaan);

        final ZoomImageView zoomImage = findViewById(R.id.zoom_image);
        Drawable img1 = getResources().getDrawable( R.drawable.petunjuk2 );
        zoomImage.setImageDrawable(img1);
    }
}
